#!/bin/bash

source /i9mlib-6.0.sh

: ${CONTEXT_SIZE="2048"}
: ${NGL="99"}

cd /tmp

if [[ "${MODEL_NAME}" == "phi-3-medium" ]]; then
  MODEL_FILE=Phi-3-medium-128k-instruct-Q5_K_M.gguf
  if [ ! -f ${MODEL_FILE} ]; then
    echo "Модель не найдена, начинаю загрузку модели..."
    wget -q https://huggingface.co/bartowski/Phi-3-medium-128k-instruct-GGUF/resolve/main/${MODEL_FILE}
  fi
fi

eval $(~/.linuxbrew/bin/brew shellenv)
llama-server -m ${MODEL_FILE} -ngl ${NGL} -fa -c ${CONTEXT_SIZE} --host 0.0.0.0 --port 80 &
pid="$!"
waitPids ${pid}