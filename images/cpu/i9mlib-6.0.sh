#!/bin/bash

# Docker API commands

function getCurNodeId {
  # везде ориентируемся на SWARM_NODE_ID, которую не прокидывает старый img
  if [[ ! -z "${SWARM_NODE_ID}" ]]; then
    RET_VAL=${SWARM_NODE_ID}
  else
    RET_VAL=$(curl --unix-socket /var/run/docker.sock -sX GET http://1.41/info | jq -r '.Swarm.NodeID')
  fi
}

function getCurNodeIp {
  # везде ориентируемся на SWARM_NODE_ID, которую не прокидывает старый img
  if [[ ! -z "${SWARM_NODE_ID}" ]]; then
    RET_VAL=${SWARM_NODE_IP}
  else
    RET_VAL=$(curl --unix-socket /var/run/docker.sock -sX GET http://1.41/info | jq -r '.Swarm.NodeAddr')
  fi
}

function getVolumes {
  # везде ориентируемся на SWARM_NODE_ID, которую не прокидывает старый img
  if [[ ! -z "${SWARM_NODE_ID}" ]]; then
    RET_VAL=${VOLUMES}
  else
    selector=".Mounts[] | select(.Type == \"volume\") | .Destination"
    RET_VAL=$(curl --unix-socket /var/run/docker.sock -sX GET http:/v1.41/containers/${CONTAINER_NAME}/json | jq -r "${selector}" || true)
  fi
}

# Entrypoint

function start {
  emptyData=false
  firstStart=false
  changeNode=false

  if [[ ! -z "${STORAGE_SERVICE}" ]]; then
    getCurNodeId
    curNode=$RET_VAL

    if [[ ! "${curNode}" =~ [a-z0-9]{24,26} ]]; then
      echo "[IMAGENARIUM]: Strange curNode name: ${curNode}. Exiting."
      sleep 5
      exit -1
    fi

    processVirtualVolumes

    storeAndGet "curNode" $curNode
    prevNode=$RET_VAL

    getVolumes
    volumes=$RET_VAL

    echo "[IMAGENARIUM]: curNode: $curNode, prevNode: $prevNode"
    echo "[IMAGENARIUM]: current container name: $CONTAINER_NAME"
    echo "[IMAGENARIUM]: detected volumes: $volumes"

    if [ -z "${prevNode}" ]; then
      firstStart=true
      if [ "${DELETE_DATA}" == "true" ]; then
        echo "[IMAGENARIUM]: First run. Remove stale data directory."
        deleteDirs "$volumes"
        emptyData=true
      fi
    else
      if [[ ! "${prevNode}" =~ [a-z0-9]{24,26} ]]; then
        echo "[IMAGENARIUM]: Strange prevNode name: ${prevNode}. Exiting."
        sleep 5
        exit -1
      fi

      if [[ $curNode != $prevNode ]]; then
        echo "[IMAGENARIUM]: Current nodeId($curNode) not equals previous nodeId($prevNode)."

        if env | grep virtual_volume; then
          echo "[IMAGENARIUM]: Virtual volumes detected! Can't delete stale data directory!"
        else
          echo "[IMAGENARIUM]: Remove stale data directory."
          deleteDirs "$volumes"
          emptyData=true
        fi

        changeNode=true
      fi
    fi
  fi

  : ${IMAGENARIUM_ADMIN_MODE="false"}

  export EMPTY_DATA=$emptyData
  export FIRST_START=$firstStart
  export CHANGE_NODE=$changeNode

  if [[ "${IMAGENARIUM_ADMIN_MODE}" == "false" ]]; then
    echo "[IMAGENARIUM]: Starting app in normal mode..."
    exec /run.sh $@
  else
    if [[ "${IMAGENARIUM_RUN_APP}" == "true" ]]; then
      echo "[IMAGENARIUM]: Starting app in admin mode..."

      /run.sh $@ &
      pid="$!"

      trap "echo '[Imagenarium]: Exiting shell. Terminate child process: ${pid}'; kill -15 ${pid}; wait ${pid}; exit 143" SIGTERM
      tail -f /dev/null & wait ${!}
    else
      echo "[IMAGENARIUM]: Running container without app..."

      trap "exit 143" SIGTERM
      tail -f /dev/null & wait ${!}
    fi
  fi
}

function deleteDirs {
  dirs=$1

  IFS=' ' read -ra DIRS <<< $(echo $dirs)

  for i in ${!DIRS[@]}; do
    if [[ ${DIRS[$i]} != *"docker.sock"* ]]; then
      echo "[IMAGENARIUM]: delete all data in dir: ${DIRS[$i]}"
      rm -rf ${DIRS[$i]}/*
    fi
  done
}

# Storage service ----------

function storeAndGet {
  requestStorage "/put/${SERVICE_NAME}/$1?value=$2"
}

function findExternalIpByNodeId {
  nodeId=$1

  if [ -z ${nodeId} ]; then
    getCurNodeId
    nodeId=$RET_VAL
  fi

  requestStorage "/nodes/externalIp/node/${nodeId}"
}

function findExternalIpByServiceName {
  serviceName=$1

  : ${serviceName=${SERVICE_NAME}}

  requestStorage "/nodes/externalIp/service/${serviceName}"
}

function requestStorage {
  requestUrl=$1

  if [[ ! "${STORAGE_SERVICE}" ]]; then
    echo >&2 "[IMAGENARIUM]: You need to specify STORAGE_SERVICE"
    exit 0
  fi

  while true; do
    echo "[IMAGENARIUM]: Try to connect to storage service: ${STORAGE_SERVICE} with url: $requestUrl"

    : ${STORAGE_PORT:=8080}

    RET_VAL=$(curl -fX GET http://$STORAGE_SERVICE:${STORAGE_PORT}${requestUrl} 2>/dev/null)

    status=$?

    if [ $status -ne 0 ]; then
      echo "[IMAGENARIUM]: Can't connect to storage service..."
    else
      echo "[IMAGENARIUM]: Response from storage service: $RET_VAL"
      break
    fi

    sleep 3
  done
}

# Utils ----------

function waitPids {
  IFS=' ' read -ra pidArray <<< "$@"

  pids="$@"

  trap "echo '[Imagenarium]: Exiting shell. Terminate child processes: ${pids}'; kill -15 ${pids}" SIGTERM SIGINT

  for pid in "${pidArray[@]}"; do
    wait "${pid}"
    echo "Process with pid ${pid} exited"
  done
}

function waitPort {
  port=$1

  while true; do
    echo "[IMAGENARIUM]: Checking TCP status for: ${port}"

    nc -zw3 127.0.0.1 $port

    status=$?

    if [ "$status" == "0" ]; then
      echo "[IMAGENARIUM]: Success"
      break
    fi

    sleep 1
  done
}

function waitHost {
  while ! host $1; do
    echo >&2 "[IMAGENARIUM]: Waiting for resolve hostname: $1"
    sleep 3
  done
}

# Virtual Volumes ----------

function processVirtualVolumes {
  env | grep virtual_volume | cut -d "=" -f2- | while read -r volume
  do
    getCurNodeIp
    ip=$RET_VAL

    echo "[IMAGENARIUM]: Try to unmount volume: ${volume}..."

    curl -fsXPOST http://volumecontroller:5552/api/umount/$volume
    status=$?

    if [ "${status}" == "0" ]; then
      echo "[IMAGENARIUM]: Unmount successful. Try to mount volume: ${volume}..."

      curl -fsXPOST http://${ip}:5553/api/mount/$volume
      status=$?

      if [ "${status}" == "0" ]; then
        echo "[IMAGENARIUM]: Mount successful"
      else
        echo "[IMAGENARIUM]: Mount error. Status: ${status}"
        kill $$
      fi
    else
      echo "[IMAGENARIUM]: Unmount error"
      kill $$
    fi
  done
}