<@requirement.NODE ref='llamacpp' primary='llamacpp-${namespace}' single='false' />

<@requirement.PARAM name='API_PORT' value='8088' required='false' type='port' />
<@requirement.PARAM name='MODEL_NAME' type='select' values='phi-3-medium' />
<@requirement.PARAM name='CONTEXT_SIZE' value='2048' />

<@img.TASK 'llamacpp-${namespace}' 'imagenarium/llamacpp-cpu:latest'>
  <@img.NODE_REF 'llamacpp' />
  <@img.VOLUME '/tmp' />
  <@img.PORT PARAMS.API_PORT '80' />
  <@img.ENV 'MODEL_NAME' PARAMS.MODEL_NAME />
  <@img.ENV 'CONTEXT_SIZE' PARAMS.CONTEXT_SIZE />
  <@img.CHECK_PORT '80' />
</@img.TASK>
